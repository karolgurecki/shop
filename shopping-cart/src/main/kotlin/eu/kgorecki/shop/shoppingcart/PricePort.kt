package eu.kgorecki.shop.shoppingcart

import arrow.core.Either
import java.math.BigDecimal

interface PricePort {
    fun calculateShoppingCartPrice(shoppingCartPriceQuery: ShoppingCartPriceQuery): Either<Exception,CalculatedPrice>

}

data class CalculatedPrice(val price:BigDecimal) {
    fun mapToCheckoutSummary(): CheckoutSummary = CheckoutSummary(price)
}

data class ShoppingCartPriceQuery(val items: Map<String, Int>)
