package eu.kgorecki.shop.shoppingcart

import arrow.core.Either

class Facade(private val service:Service) {
    fun checkout(checkoutCommand: CheckoutCommand): Either<Exception,CheckoutSummary> = service.checkout(checkoutCommand)
}
