package eu.kgorecki.shop.shoppingcart

import arrow.core.Either

class Service(private val pricePort:PricePort) {
    fun checkout(command: CheckoutCommand): Either<Exception, CheckoutSummary> = command
        .countItems()
        .calculatePrice()

    private fun Map<String, Int>.calculatePrice(): Either<Exception, CheckoutSummary> = pricePort
        .calculateShoppingCartPrice(ShoppingCartPriceQuery(this))
        .mapToCheckoutSummary()
}

private fun Either<Exception,CalculatedPrice>.mapToCheckoutSummary(): Either<Exception, CheckoutSummary> = this.map { it.mapToCheckoutSummary() }
