package eu.kgorecki.shop.shoppingcart

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration as SpringConfiguration

@SpringConfiguration("shoppingCartConfiguration")
class Configuration {

    @Bean
    fun checkoutFacade(pricePort: PricePort): Facade = Facade(Service(pricePort))
}