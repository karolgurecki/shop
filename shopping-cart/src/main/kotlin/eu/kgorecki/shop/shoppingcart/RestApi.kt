package eu.kgorecki.shop.shoppingcart

import arrow.core.getOrHandle
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class RestApi(private val facade: Facade) {

    @PostMapping("/checkout")
    fun checkout(@RequestBody() items: List<String>) = facade.checkout(CheckoutCommand(items))
        .map { ResponseEntity.ok(it) }
        .getOrHandle { ResponseEntity.badRequest().body(Error("Cannot process this request, because: ${it.message}")) } //This is done this way here for time purpose. In real scenerio I would put in body some sort of validation error data object with more useful information
}

data class Error(val message:String)