package eu.kgorecki.shop.shoppingcart

import arrow.core.Either
import java.math.BigDecimal

data class CheckoutSummary(val price:BigDecimal) {
    fun toEither(): Either<Exception, CheckoutSummary> = Either.Right(this)
}

data class CheckoutCommand(val items: List<String>) {
    fun countItems(): Map<String, Int> = items
        .groupingBy { it }
        .eachCount()
}

