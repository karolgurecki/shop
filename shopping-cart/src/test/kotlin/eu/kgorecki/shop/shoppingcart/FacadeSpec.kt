package eu.kgorecki.shop.shoppingcart

import arrow.core.Either
import arrow.core.getOrElse
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import java.math.BigDecimal

class FacadeSpec : BehaviorSpec({

    class PricePortForTest : PricePort {
        val results: MutableMap<ShoppingCartPriceQuery, Either<Exception, CalculatedPrice>> = HashMap()

        override fun calculateShoppingCartPrice(shoppingCartPriceQuery: ShoppingCartPriceQuery): Either<Exception, CalculatedPrice> =
            results.getValue(shoppingCartPriceQuery)
    }


    Given("for items list A,B,C calculated price is 12.12") {
        val items = mapOf("A" to 1, "B" to 1, "C" to 1)
        val pricePortInMenory = PricePortForTest()

        val expectedResult = BigDecimal.valueOf(12.12)
        pricePortInMenory.results[ShoppingCartPriceQuery(items)] = Either.Right(CalculatedPrice(expectedResult))

        When("trying to checkout") {
            val result =
                Configuration().checkoutFacade(pricePortInMenory).checkout(CheckoutCommand(listOf("A", "B", "C")))

            Then("returned price should 12.12") {
                result.isRight() shouldBe true

                result.getOrElse { CheckoutSummary(BigDecimal.ONE) }.price shouldBe expectedResult
            }
        }
    }
})
