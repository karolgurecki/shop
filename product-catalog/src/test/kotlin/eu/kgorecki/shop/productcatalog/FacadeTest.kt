package eu.kgorecki.shop.productcatalog

import arrow.core.getOrElse
import arrow.core.getOrHandle
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.beInstanceOf
import java.math.BigDecimal

private val defailtProductInformation = ProductInformation(BigDecimal.ZERO, 0, BigDecimal.ZERO)

class FacadeTest : BehaviorSpec({

    Given("products with ids 001, 002, 003 and 004 in database") {
        val sut = Configuration().productCatalogFacade(ProductPersistenceForTestAdapter())

        When("trying to query for product 001") {
            val result = sut.findProduct(ProductQuery("001"))

            Then("should find correct product") {
                result.isRight() shouldBe true

                result.getOrElse { defailtProductInformation } shouldBe
                        ProductInformation(
                            BigDecimal.valueOf(100), 3, BigDecimal.valueOf(200)
                        )
            }
        }

        When("trying to query for product 005") {
            val result = sut.findProduct(ProductQuery("005"))

            Then("should result in NoSuchElementException in either") {
                result.isLeft() shouldBe true

                result.getOrHandle { it } should beInstanceOf<NoSuchElementException>()
            }
        }
    }

})

class ProductPersistenceForTestAdapter : ProductPersistencePort {
    override fun findProduct(id: String): Product? = data.find { it.id == id }

    companion object {
        private val data: List<Product> = listOf(
            Product("001", "Rolex", BigDecimal.valueOf(100), 3, BigDecimal.valueOf(200)),
            Product("002", "Michael Kors", BigDecimal.valueOf(80), 2, BigDecimal.valueOf(120)),
            Product("003", "Swatch", BigDecimal.valueOf(50), null, null),
            Product("004", "Casio", BigDecimal.valueOf(30), null, null)
        )
    }
}