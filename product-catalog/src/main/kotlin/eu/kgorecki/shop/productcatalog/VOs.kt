package eu.kgorecki.shop.productcatalog

import java.math.BigDecimal


data class ProductQuery(val id: String)
data class ProductInformation(val value: BigDecimal, val minItemCountForDiscount: Int, val valueOfDiscountedSet: BigDecimal)
