package eu.kgorecki.shop.productcatalog

import eu.kgorecki.shop.finance.CatalogPort
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration as SpringConfiguration

@SpringConfiguration("productCatalogConfiguration")
class Configuration {

    @Bean
    fun productCatalogFacade(productPersistencePort: ProductPersistencePort): Facade = Facade(Service(productPersistencePort))

    @Bean
    fun financeProductCatalogAdapter(facade: Facade): CatalogPort = FinanceProductCatalogAdapter(facade)

    @Bean
    fun productPersistenceInMenoryAdapter(): ProductPersistencePort = ProductPersistenceInMenoryAdapter()
}