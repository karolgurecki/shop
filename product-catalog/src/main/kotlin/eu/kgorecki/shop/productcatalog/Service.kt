package eu.kgorecki.shop.productcatalog

import arrow.core.Either

class Service(private val productPersistencePort: ProductPersistencePort) {
    fun findProduct(command: ProductQuery): Either<Exception, ProductInformation> =
        productPersistencePort.findProduct(command.id)
            ?.toProductInformationVo()
            ?.toRightEither()
            ?: Either.Left(NoSuchElementException("Cannot find product with id ${command.id}"))
}

private fun ProductInformation.toRightEither(): Either<Exception, ProductInformation> = Either.Right(this)
