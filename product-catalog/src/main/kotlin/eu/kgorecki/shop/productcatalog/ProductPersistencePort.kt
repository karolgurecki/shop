package eu.kgorecki.shop.productcatalog

import java.math.BigDecimal

interface ProductPersistencePort {
    fun findProduct(id: String): Product?
}

data class Product(
    val id: String,
    val name: String,
    val price: BigDecimal,
    val minItemCountForDiscount: Int?,
    val valueOfDiscountedSet: BigDecimal?
) {
    fun toProductInformationVo(): ProductInformation = ProductInformation(
        value = price,
        minItemCountForDiscount = minItemCountForDiscount ?: 0,
        valueOfDiscountedSet = valueOfDiscountedSet ?: BigDecimal.ZERO
    )
}

class ProductPersistenceInMenoryAdapter : ProductPersistencePort {
    override fun findProduct(id: String): Product? = data.find { it.id == id }

    companion object {
        private val data: List<Product> = listOf(
            Product("001", "Rolex", BigDecimal.valueOf(100), 3, BigDecimal.valueOf(200)),
            Product("002", "Michael Kors", BigDecimal.valueOf(80), 2, BigDecimal.valueOf(120)),
            Product("003", "Swatch", BigDecimal.valueOf(50), null, null),
            Product("004", "Casio", BigDecimal.valueOf(30), null, null)
        )
    }
}
