package eu.kgorecki.shop.productcatalog

import arrow.core.Either

class Facade(private val service: Service) {

    fun findProduct(command: ProductQuery): Either<Exception, ProductInformation> = service.findProduct(command)
}