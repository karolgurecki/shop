package eu.kgorecki.shop.productcatalog

import arrow.core.Either
import eu.kgorecki.shop.finance.CatalogPort
import eu.kgorecki.shop.finance.ItemData
import eu.kgorecki.shop.finance.ItemQuery

class FinanceProductCatalogAdapter(private val facade: Facade) : CatalogPort {
    override fun getInformationAboutItem(query: ItemQuery): Either<Exception, ItemData> =
        facade.findProduct(ProductQuery(query.id))
            .map { it.toItemData() }
}

private fun ProductInformation.toItemData() =
    ItemData(this.value, this.minItemCountForDiscount, this.valueOfDiscountedSet)
