package eu.kgorecki.shop.launcher

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("eu.kgorecki.shop")
class ShopApplication

fun main(args: Array<String>) {
    runApplication<ShopApplication>(*args)
}
