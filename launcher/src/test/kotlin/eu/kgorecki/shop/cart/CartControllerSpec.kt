package eu.kgorecki.shop.cart

import com.fasterxml.jackson.databind.ObjectMapper
import eu.kgorecki.shop.launcher.ShopApplication
import eu.kgorecki.shop.shoppingcart.CheckoutSummary
import eu.kgorecki.shop.shoppingcart.Error
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.row
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post
import java.math.BigDecimal

@AutoConfigureMockMvc
@SpringBootTest(classes = [ShopApplication::class], webEnvironment = SpringBootTest.WebEnvironment.MOCK)
class CartControllerSpec(val mockMvc: MockMvc, val mapper: ObjectMapper) : StringSpec() {
    init {
        "should return price"{
            forAll(
                row(listOf("001"), CheckoutSummary(BigDecimal.valueOf(100))),
                row(listOf("001", "001", "001"), CheckoutSummary(BigDecimal.valueOf(200))),
                row(
                    listOf("001", "002", "001", "004", "003"), CheckoutSummary(BigDecimal.valueOf(360))
                )
            )
            { requestData, expectedResult ->
                mockMvc.post("/checkout") {
                    contentType = MediaType.APPLICATION_JSON
                    content = mapper.writeValueAsString(requestData)
                    accept = MediaType.APPLICATION_JSON
                }.andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    content { json(mapper.writeValueAsString(expectedResult)) }
                }
            }
        }

        "should return Bad Request if product on the lisst do not exists"{
            forAll(
                row(listOf("005"), Error("Cannot process this request, because: Cannot find product with id 005"))
            )
            { requestData, expectedResult ->
                mockMvc.post("/checkout") {
                    contentType = MediaType.APPLICATION_JSON
                    content = mapper.writeValueAsString(requestData)
                    accept = MediaType.APPLICATION_JSON
                }.andExpect {
                    status { isBadRequest() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    content { json(mapper.writeValueAsString(expectedResult)) }
                }
            }
        }

    }
}