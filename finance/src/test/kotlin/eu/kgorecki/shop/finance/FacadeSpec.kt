package eu.kgorecki.shop.finance

import arrow.core.Either
import arrow.core.getOrElse
import arrow.core.getOrHandle
import arrow.core.left
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.beInstanceOf
import java.math.BigDecimal

class FacadeSpec : BehaviorSpec({

    class CatalogInMemoryAdapter : CatalogPort {
        val results: MutableMap<ItemQuery, ItemData> = HashMap()

        override fun getInformationAboutItem(query: ItemQuery): Either<Exception, ItemData> {
            return if (results.contains(query)) Either.Right(results.getValue(query))
            else Either.Left(NoSuchElementException())
        }
    }

    Given("ITEM_A which have price 200 and no discount, ITEM_B have price 100.50 and discount of 149.99 for 2") {
        val catalogPort = CatalogInMemoryAdapter()

        catalogPort.results[ItemQuery("ITEM_A")] = ItemData(BigDecimal.valueOf(200), 0, BigDecimal.ZERO)
        catalogPort.results[ItemQuery("ITEM_B")] = ItemData(BigDecimal.valueOf(100.5), 2, BigDecimal.valueOf(149.99))

        val sut = Configuration().financeFacade(catalogPort)

        When("trying to calculate sum for one ITEM_A") {
            val result = sut.calculateSum(CalculateOverallSumCommand(mapOf("ITEM_A" to 1)))

            Then("calculated sum should be equal to 200") {
                result.isRight() shouldBe true

                result.getOrElse { CalculationResult(BigDecimal.ZERO) } shouldBe
                        CalculationResult(BigDecimal.valueOf(200))
            }
        }

        When("trying to calculate sum for one ITEM_A and one ITEM_B") {
            val result = sut.calculateSum(CalculateOverallSumCommand(mapOf("ITEM_A" to 1, "ITEM_B" to 1)))

            Then("calculated sum should be equal to 300.5") {
                result.isRight() shouldBe true

                result.getOrElse { CalculationResult(BigDecimal.ZERO) } shouldBe
                        CalculationResult(BigDecimal.valueOf(30050, 2))
            }
        }

        When("trying to calculate sum for two ITEM_A and five ITEM_B") {
            val result = sut.calculateSum(CalculateOverallSumCommand(mapOf("ITEM_A" to 2, "ITEM_B" to 5)))

            Then("calculated sum should be equal to 800.48") {//(2*200) + ((4*149,99)+150) = 800,48
                result.isRight() shouldBe true

                result.getOrElse { CalculationResult(BigDecimal.ZERO) } shouldBe
                        CalculationResult(BigDecimal.valueOf(800.48))
            }
        }

        When("trying to calculate sum for two ITEM_A and one ITEM_C") {
            val result = sut.calculateSum(CalculateOverallSumCommand(mapOf("ITEM_A" to 2, "ITEM_C" to 1)))

            Then("should return either with NoSuchElementException") {
                result.isLeft() shouldBe true

                result.getOrHandle { it }should beInstanceOf<NoSuchElementException>()
            }
        }
    }
})
