package eu.kgorecki.shop.finance

import arrow.core.Either
import java.math.BigDecimal

interface CatalogPort {

    fun getInformationAboutItem(query: ItemQuery): Either<Exception, ItemData>
}

data class ItemQuery(val id: String)
data class ItemData(val value: BigDecimal, val minItemCountForDiscount: Int, val valueOfDiscountedSet: BigDecimal)

