package eu.kgorecki.shop.finance

import arrow.core.Either
import eu.kgorecki.shop.shoppingcart.CalculatedPrice
import eu.kgorecki.shop.shoppingcart.PricePort
import eu.kgorecki.shop.shoppingcart.ShoppingCartPriceQuery

internal class ShoppingCartPriceAdapter(private val facade: Facade) : PricePort {
    override fun calculateShoppingCartPrice(query: ShoppingCartPriceQuery): Either<Exception, CalculatedPrice> =
        facade
            .calculateSum(CalculateOverallSumCommand(query.items))
            .map { CalculatedPrice(it.result) }
}