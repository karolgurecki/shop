package eu.kgorecki.shop.finance

import arrow.core.Either

class Facade(private val service: Service) {

    fun calculateSum(command: CalculateOverallSumCommand): Either<Exception, CalculationResult> = service.calculateSum(command)
}