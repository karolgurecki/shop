package eu.kgorecki.shop.finance

import arrow.core.Either
import arrow.core.getOrElse
import java.math.BigDecimal

class Service(private val catalogPort: CatalogPort) {

    fun calculateSum(command: CalculateOverallSumCommand): Either<Exception, CalculationResult> = command.items
        .map { it.addUpAllUniqueItems() }
        .fold(BIG_DECIMAL_ZERO_EITHER, sumItems())
        .toCalculationResult()

    private fun sumItems() =
        { acc: Either<Exception, BigDecimal>, either: Either<Exception, BigDecimal> ->
            if (acc.isLeft()) acc else either.map { it.plus(acc.getOrElse { BigDecimal.ZERO }) }
        }

    private fun Map.Entry<String, Int>.addUpAllUniqueItems(): Either<Exception, BigDecimal> =
        catalogPort.getInformationAboutItem(ItemQuery(this.key))
            .map { sumOfItemsWithDiscount(it).plus(itemSumWithoutDiscount(it)) }

    companion object {
        private val BIG_DECIMAL_ZERO_EITHER: Either<Exception, BigDecimal> = Either.Right(BigDecimal.ZERO)
    }

}

private fun Map.Entry<String, Int>.itemSumWithoutDiscount(it: ItemData) =
    if (it.minItemCountForDiscount == 0) this.value.toBigDecimal().multiply(it.value)
    else this.value.mod(it.minItemCountForDiscount).toBigDecimal().multiply(it.value)

private fun Map.Entry<String, Int>.sumOfItemsWithDiscount(it: ItemData) =
    countNUmberOfSetForDiscount(it).multiply(it.valueOfDiscountedSet)

private fun Map.Entry<String, Int>.countNUmberOfSetForDiscount(it: ItemData) =
    if (it.minItemCountForDiscount != 0) this.value.div(it.minItemCountForDiscount).toBigDecimal() else BigDecimal.ZERO

private fun Either<Exception, BigDecimal>.toCalculationResult(): Either<Exception, CalculationResult> =
    this.map { CalculationResult(it) }

