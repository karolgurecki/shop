package eu.kgorecki.shop.finance

import java.math.BigDecimal

data class CalculateOverallSumCommand(val items: Map<String, Int>)

data class CalculationResult(val result: BigDecimal)

