package eu.kgorecki.shop.finance

import eu.kgorecki.shop.shoppingcart.PricePort
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration as SpringConfiguration

@SpringConfiguration("financeConfiguration")
class Configuration {

    @Bean
    fun financeFacade(catalogPort: CatalogPort): Facade = Facade(Service(catalogPort))

    @Bean
    fun shoppingCartPriceAdapter(facade: Facade): PricePort = ShoppingCartPriceAdapter(facade)
}