### Requirements
* path to JDK 11 set in JAVA_HOME environmental variable
* internet connection
* project cloned to path with write permissions
* port 8080 should not be in use by any another program

### How to build
Using command line (Windows preferable this will be PowerShell) go to main folder of the project run following command:
```
.\mvnw clean package
```
Please note that build process was tested only on Windows machine in PowerShell. On linux/MacOs following command should work
```
./mvnw clean package
```

### How to run it
After build process is completed from main folder of the project go to `launcher/target` folder. In there run followwing command
```
java -jar launcher-0.0.1-SNAPSHOT.jar
```
Application  will start on and use port 8080 for http communication

### What I want to improve/change
* Create sql database where all the information will be store because at the moment test data are stored in ProductPersistenceInMenoryAdapter in product-catalog module
* Separate discount information from product information in catalog to different table/object to be able to implement more type of discount
* Span off discount calculation to different submodule as think that bundle discount will not be only type in it in the future
* Error handling 

### My approach
My approach was to have different services/modules for each autonomic path of the project. E.g. I saw that pice calculation can be separated from checkout functionality because it does not need to know how proce is really calculated only that it is, and it wants the result of this calculation. Because of that I decided to spit those parts of process to two modules with ports-adapters design pattern as a glue. In the nutshell in shopping-cart module I created `PricePort` which is an interface with function required by shopping-cart to process request from clients. Then finance module implements this port and calculating the overall proce of items in the list with algorithm which only it knows.
During price calculation process finance module needs to know about current pice and discount for given product, so it is using `CatalogPort` to get this information. This port is implemented by product-catalog module. Using `ProductPersistencePort` will retrieve this information for the database without need to know what if any database engine is use by the system.    